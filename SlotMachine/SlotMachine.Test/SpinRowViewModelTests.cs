﻿using NUnit.Framework;
using SlotMachine.ViewModels;
using SlotMachineBusiness.Business;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace SlotMachine.Test
{
    public class SpinRowViewModelTests
    {
        private List<SlotSymbol> DefaultPossibleSymbols = new List<SlotSymbol>()
        {
            new SlotSymbol() { Name = "Apple", Coefficient = 0.4, Probability = 45, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Banana", Coefficient = 0.6, Probability = 35, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Pineapple", Coefficient = 0.8, Probability = 15, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Wildcard", Coefficient = 0, Probability = 5, ImageBase64 = "Test" }
        };

        [TestCase]
        public void SlotMachine_SpinRowViewModel_TestParametizedConstructor_RowSymbolsIsNotNull()
        {
            //Arrange
            SpinRowResult rowResult = new SpinRowResult()
            {
                RowWinnings = 0,
                RowSymbols = new List<SlotSymbol>()
                {
                    DefaultPossibleSymbols[0], DefaultPossibleSymbols[0], DefaultPossibleSymbols[1]
                }
            };

            //Act
            SlotRowViewModel rowVm = new SlotRowViewModel(rowResult);

            //Assert
            Assert.IsNotNull(rowVm.RowSymbols);
        }

        [TestCase(3)]
        public void SlotMachine_SpinRowViewModel_TestParametizedConstructor_RowSymbolsCount(int expectedCount)
        {
            //Arrange
            SpinRowResult rowResult = new SpinRowResult()
            {
                RowWinnings = 0,
                RowSymbols = new List<SlotSymbol>()
                {
                    DefaultPossibleSymbols[0], DefaultPossibleSymbols[0], DefaultPossibleSymbols[1]
                }
            };

            //Act
            SlotRowViewModel rowVm = new SlotRowViewModel(rowResult);

            //Assert
            Assert.IsTrue(rowVm.RowSymbols.Count == expectedCount);
        }

        [TestCase(25.99)]
        public void SlotMachine_SpinRowViewModel_TestParametizedConstructor_RowWinnings(double expectedWinnings)
        {
            //Arrange
            SpinRowResult rowResult = new SpinRowResult()
            {
                RowWinnings = 25.99,
                RowSymbols = new List<SlotSymbol>()
                {
                    DefaultPossibleSymbols[0], DefaultPossibleSymbols[0], DefaultPossibleSymbols[1]
                }
            };

            //Act
            SlotRowViewModel rowVm = new SlotRowViewModel(rowResult);

            //Assert
            Assert.IsTrue(rowVm.RowWinnings == expectedWinnings);
        }
    }
}
