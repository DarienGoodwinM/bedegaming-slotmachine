﻿using NUnit.Framework;
using SlotMachine.ViewModels;
using SlotMachineBusiness.Business;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace SlotMachine.Test
{
    public class MainViewModelTests
    {
        private List<SlotSymbol> DefaultPossibleSymbols = new List<SlotSymbol>()
        {
            new SlotSymbol() { Name = "Apple", Coefficient = 0.4, Probability = 45, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Banana", Coefficient = 0.6, Probability = 35, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Pineapple", Coefficient = 0.8, Probability = 15, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Wildcard", Coefficient = 0, Probability = 5, ImageBase64 = "Test" }
        };

        #region ConstructorTests
        [TestCase(0)]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_CurrentBalance(double defaultBalance)
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsTrue(mainVm.CurrentBalance == defaultBalance);
        }

        [TestCase(0)]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_CurrentBet(double defaultBet)
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsTrue(mainVm.CurrentBet == defaultBet);
        }

        [TestCase(true)]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_BalanceIsEmpty(bool defaultBalanceIsEmpty)
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsTrue(mainVm.BalanceIsEmpty == defaultBalanceIsEmpty);
        }

        [TestCase(false)]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_UserWin(bool defaultUserWin)
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsTrue(mainVm.UserWin == defaultUserWin);
        }

        [TestCase(0)]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_ChangeInBalance(double defaultChangeInBalance)
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsTrue(mainVm.ChangeInBalance == defaultChangeInBalance);
        }

        [TestCase(false)]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_BetIsAdequate(bool defaultBetIsAdequate)
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsTrue(mainVm.BetIsAdequate == defaultBetIsAdequate);
        }

        [TestCase]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_RowOneSlotsIsNull()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsNull(mainVm.RowOneSlots);
        }

        [TestCase]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_RowTwoSlotsIsNull()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsNull(mainVm.RowTwoSlots);
        }

        [TestCase]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_RowThreeSlotsIsNull()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsNull(mainVm.RowThreeSlots);
        }

        [TestCase]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_RowFourSlotsIsNull()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsNull(mainVm.RowFourSlots);
        }

        [TestCase]
        public void SlotMachine_TestDefaultConstructor_TestDefaultVariableValues_Command_SpinTypeof()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);

            //Assert
            Assert.IsInstanceOf(typeof(ICommand), mainVm.Command_Spin);
        }
        #endregion

        #region SpinTests
        [TestCase]
        public void SlotMachine_TestSpinMethod_RowOneSlotsType()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);
            mainVm.Spin();

            //Assert
            Assert.IsInstanceOf(typeof(SlotRowViewModel), mainVm.RowOneSlots);
        }

        [TestCase]
        public void SlotMachine_TestSpinMethod_RowTwoSlotsType()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);
            mainVm.Spin();

            //Assert
            Assert.IsInstanceOf(typeof(SlotRowViewModel), mainVm.RowTwoSlots);
        }

        [TestCase]
        public void SlotMachine_TestSpinMethod_RowThreeSlotsType()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);
            mainVm.Spin();

            //Assert
            Assert.IsInstanceOf(typeof(SlotRowViewModel), mainVm.RowThreeSlots);
        }

        [TestCase]
        public void SlotMachine_TestSpinMethod_RowFourSlotsType()
        {
            //Arrange
            MainViewModel mainVm = null;
            SlotBusiness slotBusiness = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Act
            mainVm = new MainViewModel(slotBusiness);
            mainVm.Spin();

            //Assert
            Assert.IsInstanceOf(typeof(SlotRowViewModel), mainVm.RowFourSlots);
        }

        //These tests provide a specified seed so that the outcome is always the same. If the code to calculate the outcome of a spin is changed, these tests
        //may be effected, helping identify bugs.
        [TestCase(2, 10, 100, 0)] //0 + 0 + 0 + 4
        [TestCase(73, 10, 100, 0)] //0 + 0 + 0 + 0
        [TestCase(534534576, 10, 100, 0)] //0 + 0 + 12 + 0
        [TestCase(44124123, 10, 100, 12)] //12 + 0 + 12 + 0
        [TestCase(2, 100, 100, 0)] //0 + 0 + 0 + 40
        [TestCase(73, 75, 100, 0)] //0 + 0 + 0 + 0
        [TestCase(534534576, 66, 100, 0)] //0 + 0 + 79.2 + 0
        [TestCase(44124123, 75, 100, 90)] //90 + 0 + 90 + 0
        public void SlotMachine_TestSpinMethod_RowOneSlotsWinnings(int seed, double bet, double startingBalance, double expectedRowWinnings)
        {
            //Arrange
            Random random = new Random(seed);
            SlotBusiness sB = new SlotBusiness(DefaultPossibleSymbols, random);

            MainViewModel mainVm = new MainViewModel(sB);
            mainVm.CurrentBalance = startingBalance;
            mainVm.CurrentBet = bet;

            //Act
            mainVm.Spin();

            //Assert
            Assert.AreEqual(Math.Round(mainVm.RowOneSlots.RowWinnings, 2), expectedRowWinnings);
        }

        [TestCase(2, 10, 100, 0)] //0 + 0 + 0 + 4
        [TestCase(73, 10, 100, 0)] //0 + 0 + 0 + 0
        [TestCase(534534576, 10, 100, 0)] //0 + 0 + 12 + 0
        [TestCase(44124123, 10, 100, 0)] //12 + 0 + 12 + 0
        [TestCase(2, 100, 100, 0)] //0 + 0 + 0 + 40
        [TestCase(73, 75, 100, 0)] //0 + 0 + 0 + 0
        [TestCase(534534576, 66, 100, 0)] //0 + 0 + 79.2 + 0
        [TestCase(44124123, 75, 100, 0)] //90 + 0 + 90 + 0
        public void SlotMachine_TestSpinMethod_RowTwoSlotsWinnings(int seed, double bet, double startingBalance, double expectedRowWinnings)
        {
            //Arrange
            Random random = new Random(seed);
            SlotBusiness sB = new SlotBusiness(DefaultPossibleSymbols, random);

            MainViewModel mainVm = new MainViewModel(sB);
            mainVm.CurrentBalance = startingBalance;
            mainVm.CurrentBet = bet;

            //Act
            mainVm.Spin();

            //Assert
            Assert.AreEqual(Math.Round(mainVm.RowTwoSlots.RowWinnings, 2), expectedRowWinnings);
        }

        [TestCase(2, 10, 100, 0)] //0 + 0 + 0 + 4
        [TestCase(73, 10, 100, 0)] //0 + 0 + 0 + 0
        [TestCase(534534576, 10, 100, 12)] //0 + 0 + 12 + 0
        [TestCase(44124123, 10, 100, 12)] //12 + 0 + 12 + 0
        [TestCase(2, 100, 100, 0)] //0 + 0 + 0 + 40
        [TestCase(73, 75, 100, 0)] //0 + 0 + 0 + 0
        [TestCase(534534576, 66, 100, 79.2)] //0 + 0 + 79.2 + 0
        [TestCase(44124123, 75, 100, 90)] //90 + 0 + 90 + 0
        public void SlotMachine_TestSpinMethod_RowThreeSlotsWinnings(int seed, double bet, double startingBalance, double expectedRowWinnings)
        {
            //Arrange
            Random random = new Random(seed);
            SlotBusiness sB = new SlotBusiness(DefaultPossibleSymbols, random);

            MainViewModel mainVm = new MainViewModel(sB);
            mainVm.CurrentBalance = startingBalance;
            mainVm.CurrentBet = bet;

            //Act
            mainVm.Spin();

            //Assert
            Assert.AreEqual(Math.Round(mainVm.RowThreeSlots.RowWinnings, 2), expectedRowWinnings);
        }

        [TestCase(2, 10, 100, 4)] //0 + 0 + 0 + 4
        [TestCase(73, 10, 100, 0)] //0 + 0 + 0 + 0
        [TestCase(534534576, 10, 100, 0)] //0 + 0 + 12 + 0
        [TestCase(44124123, 10, 100, 0)] //12 + 0 + 12 + 0
        [TestCase(2, 100, 100, 40)] //0 + 0 + 0 + 40
        [TestCase(73, 75, 100, 0)] //0 + 0 + 0 + 0
        [TestCase(534534576, 66, 100, 0)] //0 + 0 + 79.2 + 0
        [TestCase(44124123, 75, 100, 0)] //90 + 0 + 90 + 0
        public void SlotMachine_TestSpinMethod_RowFourSlotsWinnings(int seed, double bet, double startingBalance, double expectedRowWinnings)
        {
            //Arrange
            Random random = new Random(seed);
            SlotBusiness sB = new SlotBusiness(DefaultPossibleSymbols, random);

            MainViewModel mainVm = new MainViewModel(sB);
            mainVm.CurrentBalance = startingBalance;
            mainVm.CurrentBet = bet;

            //Act
            mainVm.Spin();

            //Assert
            Assert.AreEqual(Math.Round(mainVm.RowFourSlots.RowWinnings, 2), expectedRowWinnings);
        }

        [TestCase(2, 10, 100, -6, 94)] //0 + 0 + 0 + 4
        [TestCase(73, 10, 100, -10, 90)] //0 + 0 + 0 + 0
        [TestCase(534534576, 10, 100, 2, 102)] //0 + 0 + 12 + 0
        [TestCase(44124123, 10, 100, 14, 114)] //12 + 0 + 12 + 0
        [TestCase(2, 100, 100, -60, 40)] //0 + 0 + 0 + 40
        [TestCase(73, 75, 100, -75, 25)] //0 + 0 + 0 + 0
        [TestCase(534534576, 66, 100, 13.2, 113.2)] //0 + 0 + 79.2 + 0
        [TestCase(44124123, 75, 100, 105, 205)] //90 + 0 + 90 + 0
        public void SlotMachine_TestSpinMethod_ChangeInBalanceChange(int seed, double bet, double startingBalance, double expectedChangeInBalance, double expectedNewBalance)
        {
            //Arrange
            Random random = new Random(seed);
            SlotBusiness sB = new SlotBusiness(DefaultPossibleSymbols, random);

            MainViewModel mainVm = new MainViewModel(sB);
            mainVm.CurrentBalance = startingBalance;
            mainVm.CurrentBet = bet;

            //Act
            mainVm.Spin();

            //Assert
            Assert.IsTrue(Math.Round(mainVm.ChangeInBalance, 2) == expectedChangeInBalance);
            Assert.IsTrue(Math.Round(mainVm.CurrentBalance, 2) == expectedNewBalance);
        }

        [TestCase(2, 10, 100, false)] 
        [TestCase(73, 10, 100, false)]
        [TestCase(534534576, 10, 100, true)] 
        [TestCase(44124123, 10, 100, true)] 
        [TestCase(2, 100, 100, false)] 
        [TestCase(73, 75, 100, false)] 
        [TestCase(534534576, 66, 100, true)] 
        [TestCase(44124123, 75, 100, true)] 
        public void SlotMachine_TestSpinMethod_UserWin(int seed, double bet, double startingBalance, bool expectedUserWin)
        {
            //Arrange
            Random random = new Random(seed);
            SlotBusiness sB = new SlotBusiness(DefaultPossibleSymbols, random);

            MainViewModel mainVm = new MainViewModel(sB);
            mainVm.CurrentBalance = startingBalance;
            mainVm.CurrentBet = bet;

            //Act
            mainVm.Spin();

            //Assert
            Assert.AreEqual(mainVm.UserWin, expectedUserWin);
        }

        #endregion

    }
}
