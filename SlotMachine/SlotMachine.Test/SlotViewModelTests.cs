﻿using NUnit.Framework;
using SlotMachine.ViewModels;
using SlotMachineBusiness.Business;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace SlotMachine.Test
{
    public class SlotViewModelTests
    {
        [TestCase("Apple", 0.4, 30, "Test")]
        [TestCase("Pineapple", 2.5, 1, "Test")]
        [TestCase(null, 0.1, 30, "Test")]
        public void SlotMachine_SlotViewModelTests_TestParametizedConstructor_SlotSymbolName(string name, double coefficient, double probability, string image64)
        {
            //Arrange
            SlotSymbol slotSymbol = new SlotSymbol()
            {
                Name = name,
                Coefficient = coefficient,
                Probability = probability,
                ImageBase64 = image64
            };

            //Act
            SlotViewModel slotVm = new SlotViewModel(slotSymbol);

            //Assert
            Assert.AreEqual(slotVm.Symbol.Name, name);
        }

        [TestCase("Apple", 0.4, 30, "Test")]
        [TestCase("Pineapple", 2.5, 1, "Test")]
        [TestCase(null, 0.1, 30, "Test")]
        public void SlotMachine_SlotViewModelTests_TestParametizedConstructor_SlotSymbolCoefficient(string name, double coefficient, double probability, string image64)
        {
            //Arrange
            SlotSymbol slotSymbol = new SlotSymbol()
            {
                Name = name,
                Coefficient = coefficient,
                Probability = probability,
                ImageBase64 = image64
            };

            //Act
            SlotViewModel slotVm = new SlotViewModel(slotSymbol);

            //Assert
            Assert.AreEqual(slotVm.Symbol.Coefficient, coefficient);
        }

        [TestCase("Apple", 0.4, 30, "Test")]
        [TestCase("Pineapple", 2.5, 1, "Test")]
        [TestCase(null, 0.1, 30, "Test")]
        public void SlotMachine_SlotViewModelTests_TestParametizedConstructor_SlotSymbolProbability(string name, double coefficient, double probability, string image64)
        {
            //Arrange
            SlotSymbol slotSymbol = new SlotSymbol()
            {
                Name = name,
                Coefficient = coefficient,
                Probability = probability,
                ImageBase64 = image64
            };

            //Act
            SlotViewModel slotVm = new SlotViewModel(slotSymbol);

            //Assert
            Assert.AreEqual(slotVm.Symbol.Probability, probability);
        }

        [TestCase("Apple", 0.4, 30, "Test")]
        [TestCase("Pineapple", 2.5, 1, "Test")]
        [TestCase(null, 0.1, 30, "Test")]
        public void SlotMachine_SlotViewModelTests_TestParametizedConstructor_SlotSymbolImageBase64(string name, double coefficient, double probability, string image64)
        {
            //Arrange
            SlotSymbol slotSymbol = new SlotSymbol()
            {
                Name = name,
                Coefficient = coefficient,
                Probability = probability,
                ImageBase64 = image64
            };

            //Act
            SlotViewModel slotVm = new SlotViewModel(slotSymbol);

            //Assert
            Assert.AreEqual(slotVm.Symbol.ImageBase64, image64);
        }
    }
}
