﻿using NUnit.Framework;
using SlotMachineBusiness.Business;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SlotMachineBusiness.Test
{
    public class SlotBusinessTests
    {
        private List<SlotSymbol> DefaultPossibleSymbols = new List<SlotSymbol>()
        {
            new SlotSymbol() { Name = "Apple", Coefficient = 0.4, Probability = 45, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Banana", Coefficient = 0.6, Probability = 35, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Pineapple", Coefficient = 0.8, Probability = 15, ImageBase64 = "Test" },
            new SlotSymbol() { Name = "Wildcard", Coefficient = 0, Probability = 5, ImageBase64 = "Test" }
        };

        #region ConstructorTests

        #region DefaultConstructor
        [TestCase(4)]
        public void SlotBusiness_TestDefaultConstructor_PossbileSymbolsCount(int expectedCount)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Assert
            Assert.IsTrue(businessObj.PossibleSymbols.Count == expectedCount);
        }

        [TestCase("Apple")]
        [TestCase("Banana")]
        [TestCase("Pineapple")]
        [TestCase("Wildcard")]
        public void SlotBusiness_TestDefaultConstructor_PossbileSymbolsNames(string expectedName)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(DefaultPossibleSymbols, new Random());

            //Assert
            Assert.IsTrue(businessObj.PossibleSymbols.Select(x => x.Name).Contains(expectedName));
        }

        [TestCase("Apple", 0.4)]
        [TestCase("Banana", 0.6)]
        [TestCase("Pineapple", 0.8)]
        [TestCase("Wildcard", 0)]
        public void SlotBusiness_TestDefaultConstructor_PossbileSymbolsCoefficients(string name, double expectedCoefficient)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(DefaultPossibleSymbols, new Random());
            var resultList = businessObj.PossibleSymbols.Where(x => x.Name == name).ToList();

            //Assert
            Assert.IsNotNull(resultList);
            Assert.IsTrue(resultList.Count() == 1);
            Assert.IsTrue(resultList.FirstOrDefault().Coefficient == expectedCoefficient);
        }

        [TestCase("Apple", 45)]
        [TestCase("Banana", 35)]
        [TestCase("Pineapple", 15)]
        [TestCase("Wildcard", 5)]
        public void SlotBusiness_TestDefaultConstructor_PossbileSymbolsProbabilities(string name, double expectedProbability)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(DefaultPossibleSymbols, new Random());
            var resultList = businessObj.PossibleSymbols.Where(x => x.Name == name).ToList();

            //Assert
            Assert.IsNotNull(resultList);
            Assert.IsTrue(resultList.Count() == 1);
            Assert.IsTrue(resultList.FirstOrDefault().Probability == expectedProbability);
        }
        #endregion

        #region ParameterizedConstructor
        private static List<SlotSymbol> PossibleSymbols1 = new List<SlotSymbol>()
        {
            new SlotSymbol() { Name = "Orange", Coefficient = 0.4, Probability = 60, ImageBase64 = "TEST" },
            new SlotSymbol() { Name = "Peach", Coefficient = 0.6, Probability = 30, ImageBase64 = "TEST" },
            new SlotSymbol() { Name = "Pear", Coefficient = 1.0, Probability = 10, ImageBase64 = "TEST" },
        };

        private static List<SlotSymbol> PossibleSymbols2 = new List<SlotSymbol>()
        {
            new SlotSymbol() { Name = null, Coefficient = 0.4, Probability = 60, ImageBase64 = "TEST" },
            new SlotSymbol() { Name = "Peach", Coefficient = 0.6, Probability = 30, ImageBase64 = "TEST" },
            new SlotSymbol() { Name = "Pear", Coefficient = 1.0, Probability = 5, ImageBase64 = "TEST" },
            new SlotSymbol() { Name = "Apricot", Coefficient = 1.0, Probability = 5, ImageBase64 = "TEST" },
        };

        #region PossibleSymbols1_Tests
        [TestCase(3)]
        public void SlotBusiness_TestParameterizedConstructor_PossibleSymbolsCountUsingPossibleSymbols1(int expectedCount)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(PossibleSymbols1, new Random());

            //Assert
            Assert.IsTrue(businessObj.PossibleSymbols.Count == expectedCount);
        }

        [TestCase("Orange")]
        [TestCase("Peach")]
        [TestCase("Pear")]
        public void SlotBusiness_TestParameterizedConstructor_PossbileSymbolsNamesUsingPossibleSymbols1(string expectedName)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(PossibleSymbols1, new Random());

            //Assert
            Assert.IsTrue(businessObj.PossibleSymbols.Select(x => x.Name).Contains(expectedName));
        }

        [TestCase("Orange", 0.4)]
        [TestCase("Peach", 0.6)]
        [TestCase("Pear", 1.0)]
        public void SlotBusiness_TestParameterizedConstructor_PossbileSymbolsCoefficientsUsingPossibleSymbols1(string name, double expectedCoefficient)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(PossibleSymbols1, new Random());
            var resultList = businessObj.PossibleSymbols.Where(x => x.Name == name).ToList();

            //Assert
            Assert.IsNotNull(resultList);
            Assert.IsTrue(resultList.Count() == 1);
            Assert.IsTrue(resultList.FirstOrDefault().Coefficient == expectedCoefficient);
        }

        [TestCase("Orange", 60)]
        [TestCase("Peach", 30)]
        [TestCase("Pear", 10)]
        public void SlotBusiness_TestParameterizedConstructor_PossbileSymbolsProbabilitiesUsingPossibleSymbols1(string name, double expectedProbability)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(PossibleSymbols1, new Random());
            var resultList = businessObj.PossibleSymbols.Where(x => x.Name == name).ToList();

            //Assert
            Assert.IsNotNull(resultList);
            Assert.IsTrue(resultList.Count() == 1);
            Assert.IsTrue(resultList.FirstOrDefault().Probability == expectedProbability);
        }
        #endregion

        #region PossibleSymbols2_Tests
        [TestCase(4)]
        public void SlotBusiness_TestParameterizedConstructor_PossibleSymbolsCountUsingPossibleSymbols2(int expectedCount)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(PossibleSymbols2, new Random());

            //Assert
            Assert.IsTrue(businessObj.PossibleSymbols.Count == expectedCount);
        }

        [TestCase(null)]
        [TestCase("Peach")]
        [TestCase("Pear")]
        [TestCase("Apricot")]
        public void SlotBusiness_TestParameterizedConstructor_PossbileSymbolsNamesUsingPossibleSymbols2(string expectedName)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(PossibleSymbols2, new Random());

            //Assert
            Assert.IsTrue(businessObj.PossibleSymbols.Select(x => x.Name).Contains(expectedName));
        }

        [TestCase(null, 0.4)]
        [TestCase("Peach", 0.6)]
        [TestCase("Pear", 1.0)]
        [TestCase("Apricot", 1.0)]
        public void SlotBusiness_TestParameterizedConstructor_PossbileSymbolsCoefficientsUsingPossibleSymbols2(string name, double expectedCoefficient)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(PossibleSymbols2, new Random());
            var resultList = businessObj.PossibleSymbols.Where(x => x.Name == name).ToList();

            //Assert
            Assert.IsNotNull(resultList);
            Assert.IsTrue(resultList.Count() == 1);
            Assert.IsTrue(resultList.FirstOrDefault().Coefficient == expectedCoefficient);
        }

        [TestCase(null, 60)]
        [TestCase("Peach", 30)]
        [TestCase("Pear", 5)]
        [TestCase("Apricot", 5)]
        public void SlotBusiness_TestParameterizedConstructor_PossbileSymbolsProbabilitiesUsingPossibleSymbols2(string name, double expectedProbability)
        {
            //Arrange
            SlotBusiness businessObj = null;

            //Act
            businessObj = new SlotBusiness(PossibleSymbols2, new Random());
            var resultList = businessObj.PossibleSymbols.Where(x => x.Name == name).ToList();

            //Assert
            Assert.IsNotNull(resultList);
            Assert.IsTrue(resultList.Count() == 1);
            Assert.IsTrue(resultList.FirstOrDefault().Probability == expectedProbability);
        }
        #endregion

        //These tests are done to ensure that providing a specified seed for the random object will always result in the same values. This is not needed for
        //the default running of the application, it is used to help identify potenital bugs when testing the RNG functionality of the game.
        #region RandomSeed_Tests
        [TestCase(1, 25, 11, 47)]
        [TestCase(2, 77, 41, 17)]
        [TestCase(73, 86, 24, 79)]
        public void SlotBusiness_TestParametizedConstructor_RandomNextIntUsingSpecifiedSeed(int seed, int expectedFirstInt, int expectedSecondInt, int expectedThirdInt)
        {
            //Arrange
            SlotBusiness businessObj = null;
            Random testRandom = new Random(seed);

            //Act
            businessObj = new SlotBusiness(PossibleSymbols1, testRandom);
            int resultFirstInt = businessObj.random.Next(1, 100);
            int resultSecondInt = businessObj.random.Next(1, 100);
            int resultThirdInt = businessObj.random.Next(1, 100);

            //Assert
            Assert.IsTrue(expectedFirstInt == resultFirstInt);
            Assert.IsTrue(expectedSecondInt == resultSecondInt);
            Assert.IsTrue(expectedThirdInt == resultThirdInt);
        }
        #endregion

        #endregion

        #endregion

        #region SpinRowTests
        [TestCase(1, new string[] { "Banana", "Pineapple", "Banana" })] //25, 11, 47
        [TestCase(2, new string[] { "Apple", "Banana", "Pineapple" })] //77, 41, 17
        [TestCase(73, new string[] { "Apple", "Banana", "Apple" })] //86, 24, 79
        [TestCase(23443252, new string[] { "Wildcard", "Banana", "Banana" })] //2, 41, 38
        [TestCase(534534576, new string[] { "Banana", "Pineapple", "Apple" })] //54, 16, 88
        [TestCase(44124123, new string[] { "Apple", "Apple", "Apple" })] //55, 99, 83
        public void SlotBusiness_TestSpinRow_ReturnedRowSymbolsNames_UsingDefaultPossibleSymbols(int seed, string[] nameArr)
        {
            //Arrange
            SlotBusiness businessObj = null;
            Random testRandom = new Random(seed);

            //Act
            businessObj = new SlotBusiness(DefaultPossibleSymbols, testRandom);
            var result = businessObj.SpinRow(10);

            //Assert
            Assert.IsTrue(result.RowSymbols[0].Name == nameArr[0]);
            Assert.IsTrue(result.RowSymbols[1].Name == nameArr[1]);
            Assert.IsTrue(result.RowSymbols[2].Name == nameArr[2]);
        }

        [TestCase(1, 10, 0)] //"Banana", "Pineapple", "Banana"
        [TestCase(2, 10, 0)] //"Apple", "Banana", "Pineapple"
        [TestCase(73, 10, 0 )] //"Apple", "Banana", "Apple"
        [TestCase(23443252, 10, 12)] //"Wildcard", "Banana", "Banana"
        [TestCase(534534576, 10, 0)] //"Banana", "Pineapple", "Apple"
        [TestCase(44124123, 10, 12)] //"Apple", "Apple", "Apple"
        [TestCase(23443252, 100, 120)] //"Wildcard", "Banana", "Banana"
        [TestCase(44124123, 100, 120)] //"Apple", "Apple", "Apple"
        [TestCase(23443252, 150, 180)] //"Wildcard", "Banana", "Banana"
        [TestCase(44124123, 195.5, 234.6)] //"Apple", "Apple", "Apple"
        public void SlotBusiness_TestSpinRow_ReturnedRowWinnings_UsingDefaultPossibleSymbols(int seed, double bet, double expectedWinnings)
        {
            //Arrange
            SlotBusiness businessObj = null;
            Random testRandom = new Random(seed);

            //Act
            businessObj = new SlotBusiness(DefaultPossibleSymbols, testRandom);
            var result = businessObj.SpinRow(bet);

            //Assert
            Assert.IsTrue(Math.Round(result.RowWinnings, 2) == expectedWinnings);
        }

        [TestCase(1, 10, -10)] //"Banana", "Pineapple", "Banana"
        [TestCase(2, 10, -10)] //"Apple", "Banana", "Pineapple"
        [TestCase(73, 10, -10)] //"Apple", "Banana", "Apple"
        [TestCase(23443252, 10, 2)] //"Wildcard", "Banana", "Banana"
        [TestCase(534534576, 10, -10)] //"Banana", "Pineapple", "Apple"
        [TestCase(44124123, 10, 2)] //"Apple", "Apple", "Apple"
        [TestCase(23443252, 100, 20)] //"Wildcard", "Banana", "Banana"
        [TestCase(44124123, 100, 20)] //"Apple", "Apple", "Apple"
        [TestCase(23443252, 150, 30)] //"Wildcard", "Banana", "Banana"
        [TestCase(44124123, 195.5, 39.1)] //"Apple", "Apple", "Apple"
        public void SlotBusiness_TestAcquireChangeInBalance_UsingDefaultPossibleSymbols(int seed, double bet, double expectedChangeInBalance)
        {
            //Arrange
            SlotBusiness businessObj = null;
            Random testRandom = new Random(seed);

            //Act
            businessObj = new SlotBusiness(DefaultPossibleSymbols, testRandom);
            var spinResult = businessObj.SpinRow(bet);
            var acquireResult = businessObj.AcquireChangeInBalance(spinResult.RowWinnings, bet);

            //Assert
            Assert.IsTrue(Math.Round(acquireResult, 2) == expectedChangeInBalance);
        }
        #endregion
    }
}
