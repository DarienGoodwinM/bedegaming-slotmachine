namespace SlotMachineDatabaseEFModel
{
    using SlotMachineDatabaseEFModel.Tables;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Drawing;
    using System.Linq;

    public class SlotDB : DbContext
    {
        public SlotDB() : base("name=SlotDB")
        {
            System.Data.Entity.Database.SetInitializer(new DbInitializer());
        }

        public virtual DbSet<SlotItems> SlotItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SlotItems>().Property(e => e.SlotName).IsUnicode(false);
        }

        //This class creates a database if it does not exist at the location specified by SlotDB in the config.
        //Default data is seeded to the database to quickly populate it.
        private class DbInitializer : CreateDatabaseIfNotExists<SlotDB>
        {
            protected override void Seed(SlotDB context)
            {
                var slotItems = new List<SlotItems>()
                {
                   new SlotItems() { PK_SlotItemID = 1, SlotName = "Apple", SlotCoefficient = 0.4, SlotProbability = 45, SlotImageBase64 = GetImageBase64FromImage(Resource.apple) },
                   new SlotItems() { PK_SlotItemID = 2, SlotName = "Banana", SlotCoefficient = 0.6, SlotProbability = 35, SlotImageBase64 = GetImageBase64FromImage(Resource.banana) },
                   new SlotItems() { PK_SlotItemID = 3, SlotName = "Pineapple", SlotCoefficient = 0.8, SlotProbability = 15, SlotImageBase64 = GetImageBase64FromImage(Resource.pineapple) },
                   new SlotItems() { PK_SlotItemID = 4, SlotName = "Wildcard", SlotCoefficient = 0, SlotProbability = 5, SlotImageBase64 = GetImageBase64FromImage(Resource.asterisk) }
                };
                slotItems.ForEach(x => context.SlotItems.Add(x));

                context.SaveChanges();

                base.Seed(context);
            }

            /// <summary>
            /// Takes a Bitmap object and converts it to a Base64String.
            /// </summary>
            /// <param name="img"></param>
            /// <returns></returns>
            private static string GetImageBase64FromImage(Bitmap img)
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                byte[] imageBytes = stream.ToArray();

                return Convert.ToBase64String(imageBytes);
            }
        }
    }
}