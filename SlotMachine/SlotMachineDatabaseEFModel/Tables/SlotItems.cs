﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachineDatabaseEFModel.Tables
{
    public partial class SlotItems
    {
        public SlotItems() { }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PK_SlotItemID { get; set; }

        [Required]
        public string SlotName { get; set; }

        [Required]
        public double SlotCoefficient { get; set; }

        [Required]
        public double SlotProbability { get; set; }

        [Required]
        public string SlotImageBase64 { get; set; }
    }
}
