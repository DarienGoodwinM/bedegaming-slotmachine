﻿using SlotMachineBusinessServiceFacade.DTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachineBusinessServiceFacade
{
    public interface ISlotMachineServiceFacade
    {
        #region Get
        Task<IEnumerable<SF_SlotItemsDTO>> GetAllSlotItems();
        #endregion

        #region Post
        Task<SF_SlotItemsDTO> AddNewSlotItem(SF_SlotItemsDTO newSlotItem);
        #endregion
    }

    public class SlotMachineServiceFacade : ISlotMachineServiceFacade
    {
        protected HttpClient client;
        public bool IsConnected { get; set; }

        public SlotMachineServiceFacade()
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SlotMachineServiceURL"]);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        #region Get
        public async Task<IEnumerable<SF_SlotItemsDTO>> GetAllSlotItems()
        {
            if(IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/GetAllSlotItems");
                    HttpResponseMessage response = await client.GetAsync(apiString);
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<SF_SlotItemsDTO>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }
        #endregion

        #region Post
        public async Task<SF_SlotItemsDTO> AddNewSlotItem(SF_SlotItemsDTO newSlotItem)
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = await client.PostAsJsonAsync("api/AddNewSlotItem", newSlotItem);
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<SF_SlotItemsDTO>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }
        #endregion
    }
}
