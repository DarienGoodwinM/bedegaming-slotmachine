﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachineBusinessServiceFacade.DTOs
{
    /// <summary>
    /// A DTO object used to transfer from the Service layer to the Business layer of the application.
    /// </summary>
    public class SF_SlotItemsDTO
    {
        public string SlotName { get; set; }
        public double SlotCoefficient { get; set; }
        public double SlotProbability { get; set; }
        public string SlotImageBase64 { get; set; }

        /// <summary>
        /// Makes sure the constructed DTO is valid.
        /// </summary>
        public bool ValidModel
        {
            get
            {
                return !String.IsNullOrEmpty(SlotName) && SlotCoefficient >= 0 && SlotProbability > 0 &&
                    !String.IsNullOrEmpty(SlotImageBase64);
            }
        }
    }
}
