﻿using SlotMachineBusinessServiceFacade.DTOs;
using SlotMachineDatabaseEFModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SlotMachineBusinessService.Controllers
{
    public class SlotController : ApiController
    {
        private SlotDB SlotDB;

        public SlotController()
        {
            SlotDB = new SlotDB();
        }

        //By being able to provide a database to the controller, databases can quickly be switched out. This is good for switching between testing
        //and production environments.
        public SlotController(SlotDB _db)
        {
            this.SlotDB = _db;
        }

        #region Gets
        [HttpGet]
        [Route("api/GetAllSlotItems")]
        public IHttpActionResult GetAllSlotItems()
        {
            try
            {
                var requestResult = SlotDB.SlotItems.ToList();
                if (requestResult != null && requestResult.Count() > 0)
                    return Content(HttpStatusCode.OK, requestResult.Select(x => new SF_SlotItemsDTO()
                    {
                        SlotName = x.SlotName,
                        SlotCoefficient = x.SlotCoefficient,
                        SlotProbability = x.SlotProbability,
                        SlotImageBase64 = x.SlotImageBase64
                    }).ToList());
                return Content(HttpStatusCode.NotFound, "");
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.ServiceUnavailable, "");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }
        }

        #endregion

        #region Posts
        [HttpPost]
        [Route("api/AddNewSlotItem")]
        public IHttpActionResult AddNewSlotItem(SF_SlotItemsDTO newSlotItem)
        {
            try
            {
                if (newSlotItem != null && newSlotItem.ValidModel)
                {
                    var newSlotItemRecord = new SlotMachineDatabaseEFModel.Tables.SlotItems()
                    {
                        SlotCoefficient = newSlotItem.SlotCoefficient,
                        SlotImageBase64 = newSlotItem.SlotImageBase64,
                        SlotName = newSlotItem.SlotName,
                        SlotProbability = newSlotItem.SlotProbability
                    };
                    SlotDB.SlotItems.Add(newSlotItemRecord);

                    var successfulSave = SlotDB.SaveChanges();
                    if (successfulSave == 1)
                        return Content(HttpStatusCode.Created, newSlotItem);

                    return Content(HttpStatusCode.NotFound, "");
                }
                return Content(HttpStatusCode.BadRequest, "");
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.ServiceUnavailable, "");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }
        }
        #endregion

    }
}
