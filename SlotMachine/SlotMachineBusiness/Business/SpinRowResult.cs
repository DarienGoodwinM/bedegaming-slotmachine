﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachineBusiness.Business
{
    public class SpinRowResult
    {
        /// <summary>
        /// A list of all the symbols for a row in the slot machine.
        /// </summary>
        public List<SlotSymbol> RowSymbols { get; set; }

        /// <summary>
        /// The calculated winnings for the selected row.
        /// </summary>
        public double RowWinnings { get; set; }
    }
}
