﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachineBusiness.Business
{
    public class SlotSymbol
    {
        /// <summary>
        /// The name of the Symbol.
        /// </summary>
        public string Name { get; set; } //Apple, Banana, Pineapple or Wildcard

        /// <summary>
        /// The Coefficient of the symbol.
        /// </summary>
        public double Coefficient { get; set; }

        /// <summary>
        /// The probability that this symbol would be picked at random.
        /// </summary>
        public double Probability { get; set; }

        /// <summary>
        /// A base64 string of the image used for the UI of the application.
        /// </summary>
        public string ImageBase64 { get; set; }
    }
}
