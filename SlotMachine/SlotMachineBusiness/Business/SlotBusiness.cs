﻿using SlotMachineBusinessServiceFacade;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachineBusiness.Business
{
    public class SlotBusiness
    {
        private ISlotMachineServiceFacade slotMachineFacade;

        #region Constructors
        public SlotBusiness()
        {
        }

        public SlotBusiness(ISlotMachineServiceFacade _slotMachineFacade)
        {
            this.slotMachineFacade = _slotMachineFacade;
            random = new Random();
            PopulatePossibleSymbols();
        }

        public SlotBusiness(List<SlotSymbol> PossibleSymbols, Random random)
        {
            //This constructor is made for testing purposes. By allowing for the PossibleSymbols and random variables to be provided, the testing
            //suite can provide probabilites and see how the application handles additional symbols.

            //This is similar to how dependency injection can be used to change connections to databases on the fly. If a database were to have
            //been used for this task, I would have used a Mock framework to provide an instance of the required databases to this class.
            this.PossibleSymbols = PossibleSymbols;
            this.random = random; //Random is provided so that a specific seed can be provided.
        }
        #endregion

        #region Variables
        /// <summary>
        /// The random object used for selecting a random number used for selecting symbols for each row of the slot machine.
        /// </summary>
        public Random random;

        public List<SlotSymbol> PossibleSymbols { get; set; }
        #endregion

        #region Methods
        private async void PopulatePossibleSymbols()
        {
            var slotItems = await slotMachineFacade.GetAllSlotItems();
            PossibleSymbols = slotItems.Select(x => new SlotSymbol()
            {
                Name = x.SlotName,
                Coefficient = x.SlotCoefficient,
                Probability = x.SlotProbability,
                ImageBase64 = x.SlotImageBase64
            }).ToList();
        }

        /// <summary>
        /// Takes the provided Bitmap image and converts it to a Base64String (the format in which a database would store the image).
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private static string GetImageBase64FromImage(Bitmap img)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            img.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
            byte[] imageBytes = stream.ToArray();

            return Convert.ToBase64String(imageBytes);
        }

        /// <summary>
        /// Randomly generates the result of a single row (3 symbols returned). The total coefficient for the row is then calculated (if a match is detected).
        /// The method returns a object of type SpinRowResult which contains a list of all the symbol objects and the winnings from the row.
        /// </summary>
        public SpinRowResult SpinRow(double currentBet)
        {
            //Used as part of the random number generator.
            double probablityTotal = PossibleSymbols.Sum(x => x.Probability);
            List<SlotSymbol> SymbolRow = new List<SlotSymbol>();

            SymbolRow.Add(AcquireSymbol(probablityTotal));
            SymbolRow.Add(AcquireSymbol(probablityTotal));
            SymbolRow.Add(AcquireSymbol(probablityTotal));

            //Remove all Wildcard symbols from row and select all distinct names.
            var symbolNameList = SymbolRow.Where(x => x.Name != "Wildcard").Select(x => x.Name).Distinct();
            double coefficient = 0;
            //Check if the name count is 1. If so, then a match is made.
            if (symbolNameList.Count() == 1)
                coefficient = SymbolRow.Sum(x => x.Coefficient); //If so, sum all the coefficients together.

            //Returns a SpinRowResult object that contains all the row symbols and the total winnings for that row.
            return new SpinRowResult() { RowSymbols = SymbolRow, RowWinnings = coefficient * currentBet };
        }

        /// <summary>
        /// Takes a probabity total (100 in this example) and randomly generates a number. This number is then used to identify which symbol should be selected
        /// based off of each symbols probabilities.
        /// </summary>
        /// <param name="probablityTotal"></param>
        /// <returns></returns>
        private SlotSymbol AcquireSymbol(double probablityTotal)
        {
            double selectedNumber = Math.Round(random.NextDouble() * (probablityTotal - 1) + 1, 2);
            double minPosition = 0;
            double maxPosition = 0;

            foreach (SlotSymbol symbol in PossibleSymbols.OrderBy(x => x.Probability))
            {
                maxPosition += symbol.Probability;
                if (selectedNumber >= minPosition && selectedNumber <= maxPosition)
                {
                    return symbol;
                }
                minPosition = maxPosition;
            }
            return null;
        }

        /// <summary>
        /// Returns how much the current users balance will change by being provided the total winnings from the previous spin and the bet.
        /// </summary>
        /// <param name="totalWinnings"></param>
        /// <param name="currentBet"></param>
        /// <returns></returns>
        public double AcquireChangeInBalance(double totalWinnings, double currentBet)
        {
            return 0 - currentBet + totalWinnings;
        }
        #endregion
    }
}
