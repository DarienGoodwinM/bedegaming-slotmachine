﻿using SlotMachine.ViewModels.Generic;
using SlotMachineBusiness.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachine.ViewModels
{
    public class SlotViewModel : ViewModelTemplate
    {
        public SlotViewModel() { }
        public SlotViewModel(SlotSymbol _Symbol)
        {
            this.Symbol = _Symbol;
        }

        private SlotSymbol _Symbol;
        private PropertyChangedEventArgs _SymbolArgs = new PropertyChangedEventArgs("Symbol");
        public SlotSymbol Symbol
        {
            get { return _Symbol; }
            set { _Symbol = value; NotifyChange(_SymbolArgs); NotifyChange(_ImageBase64Args); } //When the SlotSymbol object is updated, the UI is notified that both the SlotSymbol object and the ImageBase64 varialbe have been updated.
        }

        private PropertyChangedEventArgs _ImageBase64Args = new PropertyChangedEventArgs("ImageBase64");
        public string ImageBase64
        {
            get { return Symbol != null ? Symbol.ImageBase64 : ""; }
        }
    }
}
