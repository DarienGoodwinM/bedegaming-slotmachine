﻿using SlotMachine.LINQExtensions;
using SlotMachine.ViewModels.Generic;
using SlotMachineBusiness.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachine.ViewModels
{
    public class SlotRowViewModel : ViewModelTemplate
    {
        public SlotRowViewModel() { }

        public SlotRowViewModel(SpinRowResult spinRowResult)
        {
            //Converts a list of SlotSymbols to a list of SlotViewModels and then to an ObservableCollection.
            this.RowSymbols = spinRowResult.RowSymbols.Select(x => new SlotViewModel(x)).ToObservableCollection();
            this.RowWinnings = spinRowResult.RowWinnings;
        }

        private ObservableCollection<SlotViewModel> _RowSymbols;
        private PropertyChangedEventArgs _RowSymbolsArgs = new PropertyChangedEventArgs("RowSymbols");
        public ObservableCollection<SlotViewModel> RowSymbols
        {
            get { return _RowSymbols; }
            set { _RowSymbols = value; NotifyChange(_RowSymbolsArgs); }
        }

        private double _RowWinnings;
        private PropertyChangedEventArgs _RowWinningsArgs = new PropertyChangedEventArgs("RowWinnings");
        public double RowWinnings
        {
            get { return _RowWinnings; }
            set { _RowWinnings = value; NotifyChange(_RowWinningsArgs); NotifyChange(_RowWinningsBoolArgs); }
        }

        private PropertyChangedEventArgs _RowWinningsBoolArgs = new PropertyChangedEventArgs("RowWinningsBool");
        public bool RowWinningsBool
        {
            get { return RowWinnings > 0; }
        }
    }
}
