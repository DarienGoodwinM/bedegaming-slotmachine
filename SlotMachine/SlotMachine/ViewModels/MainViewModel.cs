﻿using SlotMachine.EventHandlers;
using SlotMachine.ViewModels.Generic;
using SlotMachineBusiness.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SlotMachine.ViewModels
{
    public class MainViewModel : ViewModelTemplate
    {
        #region Constructor
        //To quickly change the buisiness logic layer, the SlotBusiness object could be passed to the MainViewModel as an abstract class or interface.
        //This task does not utilize depenendy injection due to its simplicity.
        public MainViewModel()
        {
            slotR = new SlotBusiness();
        }

        public MainViewModel(SlotBusiness _slotR)
        {
            slotR = _slotR;
        }
        #endregion

        private SlotBusiness slotR;

        private double _CurrentBalance;
        private PropertyChangedEventArgs _CurrentBalanceArgs = new PropertyChangedEventArgs("CurrentBalance");
        /// <summary>
        /// The current balance of the user
        /// </summary>
        public double CurrentBalance
        {
            get { return _CurrentBalance; }
            set { _CurrentBalance = value;  NotifyChange(_CurrentBalanceArgs); NotifyChange(_BetIsAdequateArgs); NotifyChange(_BalanceIsEmpty); }
        }

        private double _CurrentBet;
        private PropertyChangedEventArgs _CurrentBetArgs = new PropertyChangedEventArgs("CurrentBet");
        /// <summary>
        /// The current bet entered by the user.
        /// </summary>
        public double CurrentBet
        {
            get { return _CurrentBet; }
            set { _CurrentBet = value; NotifyChange(_CurrentBetArgs); NotifyChange(_BetIsAdequateArgs); }
        }

        #region UIFlags

        private PropertyChangedEventArgs _BalanceIsEmpty = new PropertyChangedEventArgs("BalanceIsEmpty");
        public bool BalanceIsEmpty
        {
            get
            {
                if (CurrentBalance == 0)
                {
                    ChangeInBalance = 0; //Resets change in balance.
                    return true;
                }
                return false;
            }
        }

        private bool _UserWin;
        private PropertyChangedEventArgs _UserWinArgs = new PropertyChangedEventArgs("UserWin");
        /// <summary>
        /// A flag that specifies if the user won from the previous game.
        /// </summary>
        public bool UserWin
        {
            get { return _UserWin; }
            set { _UserWin = value; NotifyChange(_UserWinArgs); }
        }

        private double _ChangeInBalance;
        private PropertyChangedEventArgs _ChangeInBalanceArgs = new PropertyChangedEventArgs("ChangeInBalance");
        /// <summary>
        /// A double that represents the change in balance after the previous spin.
        /// </summary>
        public double ChangeInBalance
        {
            get { return _ChangeInBalance; }
            set { _ChangeInBalance = value; NotifyChange(_ChangeInBalanceArgs); }
        }

        private PropertyChangedEventArgs _BetIsAdequateArgs = new PropertyChangedEventArgs("BetIsAdequate");
        /// <summary>
        /// A flag that represents if the current bet is adequate.
        /// </summary>
        public bool BetIsAdequate
        {
            get
            {
                return CurrentBalance >= CurrentBet && CurrentBet > 0;
            }
        }
        #endregion

        #region SlotRowVariables
        //To follow MVVM, the NotifyChange method needs to be called when ever a variable used for the UI is modified. This requires the PropertyChangedEventArgs
        //objects to be created that link to the variable to be changed. This occurs in the set method of each variable.
        private SlotRowViewModel _RowOneSlots;
        private PropertyChangedEventArgs _RowOneSlotsArgs = new PropertyChangedEventArgs("RowOneSlots");
        /// <summary>
        /// An object that contains the slots in  row one of the machine.
        /// </summary>
        public SlotRowViewModel RowOneSlots
        {
            get { return _RowOneSlots; }
            set { _RowOneSlots = value; NotifyChange(_RowOneSlotsArgs); }
        }

        private SlotRowViewModel _RowTwoSlots;
        private PropertyChangedEventArgs _RowTwoSlotsArgs = new PropertyChangedEventArgs("RowTwoSlots");
        /// <summary>
        /// An object that contains the slots in  row two of the machine.
        /// </summary>
        public SlotRowViewModel RowTwoSlots
        {
            get { return _RowTwoSlots; }
            set { _RowTwoSlots = value; NotifyChange(_RowTwoSlotsArgs); }
        }

        private SlotRowViewModel _RowThreeSlots;
        private PropertyChangedEventArgs _RowThreeSlotsArgs = new PropertyChangedEventArgs("RowThreeSlots");
        /// <summary>
        /// An object that contains the slots in  row three of the machine.
        /// </summary>
        public SlotRowViewModel RowThreeSlots
        {
            get { return _RowThreeSlots; }
            set { _RowThreeSlots = value; NotifyChange(_RowThreeSlotsArgs); }
        }

        private SlotRowViewModel _RowFourSlots;
        private PropertyChangedEventArgs _RowFourSlotsArgs = new PropertyChangedEventArgs("RowFourSlots");
        /// <summary>
        /// An object that contains the slots in  row four of the machine.
        /// </summary>
        public SlotRowViewModel RowFourSlots
        {
            get { return _RowFourSlots; }
            set { _RowFourSlots = value; NotifyChange(_RowFourSlotsArgs); }
        }
        #endregion

        #region Commands
        private ICommand _Command_Spin;
        /// <summary>
        /// ICommand object used to trigger the Spin method. Uses the widely used RelayCommand object.
        /// </summary>
        public ICommand Command_Spin
        {
            get
            {
                if (_Command_Spin == null) _Command_Spin = new RelayCommand(param => this.Spin());
                return _Command_Spin;
            }
        }

        /// <summary>
        /// Sets each of the RowSlot variables to a new SlotRowViewModel object using the slot randomizer SpinRow method to populate the new ViewModel.
        /// </summary>
        public void Spin()
        {
            RowOneSlots = new SlotRowViewModel(slotR.SpinRow(CurrentBet));
            RowTwoSlots = new SlotRowViewModel(slotR.SpinRow(CurrentBet));
            RowThreeSlots = new SlotRowViewModel(slotR.SpinRow(CurrentBet));
            RowFourSlots = new SlotRowViewModel(slotR.SpinRow(CurrentBet));

            double totalWinnings = RowOneSlots.RowWinnings + RowTwoSlots.RowWinnings + RowThreeSlots.RowWinnings + RowFourSlots.RowWinnings;
            ChangeInBalance = Math.Round(slotR.AcquireChangeInBalance(totalWinnings, CurrentBet), 2);
            UserWin = ChangeInBalance >= 0;

            CurrentBalance = Math.Round(CurrentBalance + ChangeInBalance, 2);
        }
        #endregion
    }
}
