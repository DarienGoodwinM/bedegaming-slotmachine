﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlotMachine.ViewModels.Generic
{
    public class ViewModelTemplate : INotifyPropertyChanged
    {
        //Property Change Notification.
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyChange(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }
    }
}
