﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SlotMachine.Converters
{
    public class Base64ToImageConverter : MarkupExtension, IValueConverter
    {
        private static Base64ToImageConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new Base64ToImageConverter();
            return _converter;
        }

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (targetType == typeof(ImageSource))
                {
                    try
                    {
                        byte[] byteBuffer = System.Convert.FromBase64String(value.ToString());
                        MemoryStream memoryStream = new MemoryStream(byteBuffer);
                        BitmapImage bitmapImage = new BitmapImage();
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource = memoryStream;
                        bitmapImage.EndInit();

                        return bitmapImage;
                    }
                    catch (Exception ex)
                    {
                        //Something went wrong with the memory stream. Return default image.
                        return SlotMachine.Properties.Resources.DefaultItemPicture;
                    }
                }
                throw new InvalidOperationException("Converter can only convert to value of type ImageSource.");
            }
            return null;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("Converter cannot convert back.");
        }
    }
}
